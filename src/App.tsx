import { useEffect, useState } from "react";
import QueryPopup from "./components/queryPopup";
import companyLogo from "./images/companyLogo.svg";
import chart from "./images/chart.svg";
import chart2 from "./images/chart2.svg";
import settings from "./images/Settings.svg";
import avatar from "./images/Avatars.svg";
import filter from "./images/filter.svg";
import search from "./images/search.svg";
import download from "./images/download.svg";
import calendar from "./images/Calendar.svg";
import deleteIcon from "./images/delete.svg";

function App() {
  const [popup, setPopup] = useState(false);
  const [queries, setQueries] = useState([]);

  useEffect(() => {
    if (sessionStorage.getItem("queries") !== null) {
      setQueries(JSON.parse(sessionStorage.getItem("queries") as string));
    }
  }, [popup]);

  const removeQuery = (query: string, index: number) => {
    const filteredQueries = queries.filter((q) => q !== query);
    setQueries(filteredQueries);
    sessionStorage.setItem("queries", JSON.stringify(filteredQueries));

    const queriesObjects = JSON.parse(
      sessionStorage.getItem("queriesObject") as string
    );
    console.log(queriesObjects, index);
    const filteredQueriesObjects = queriesObjects.filter(
      (obj: Object, i: number) => i !== index
    );
    sessionStorage.setItem(
      "queriesObject",
      JSON.stringify(filteredQueriesObjects)
    );
  };

  return (
    <div className="bg-[#1D2025]">
      <div className="bg-[#292C33] py-5 pl-28 pr-44 flex items-center justify-between">
        <div className="flex items-center justify-start">
          <img src={companyLogo} className="mr-4" alt="" />
          <p className="mr-12 text-white font-bold text-[20px]">QueryBuilder</p>
          <div className="bg-[rgba(255,255,255,0.05)] rounded-[37px] mx-[6px] py-[6px] px-[8px] flex cursor-pointer">
            <img src={chart} alt="charticon" />
            <p className="text-[white] text-[16px] font-[500] ml-2">
              Themetic Analysis
            </p>
          </div>
          <div
            className="rounded-[37px] mx-[6px] py-[6px] px-[8px] flex transition duration-300
                        hover:bg-[rgba(255,255,255,0.05)] hover:rounded-[37px] group cursor-pointer"
          >
            <img
              src={chart2}
              alt="charticon"
              className="group-hover:brightness-200"
            />
            <p className="text-[#8B8C8C] text-[16px] font-[500] ml-2 group-hover:text-white">
              Nascent Themes
            </p>
          </div>
          <div
            className="rounded-[37px] mx-[6px] py-[6px] px-[8px] flex transition duration-300
               hover:bg-[rgba(255,255,255,0.05)] hover:rounded-[37px] group cursor-pointer"
          >
            <img
              src={settings}
              alt="settings"
              className="group-hover:brightness-200"
            />
            <p className="text-[#8B8C8C] text-[16px] font-[500] ml-2 group-hover:text-white">
              Settings
            </p>
          </div>
        </div>
        <div className="flex mr-6 items-center justify-start">
          <p className="text-[#8B8C8C] font-[500] mr-4">
            Welcome to QueryBuilder!
          </p>
          <img src={avatar} alt="" />
        </div>
      </div>
      <div className="flex pt-8 pl-24">
        <div className="w-[15%] pt-[30px]">
          <div className="flex items-center justify-start">
            <img src={filter} alt="" />
            <p className="text-[white] text-[18px] font-[500] ml-1">
              Build your query
            </p>
          </div>
          <p className="text-[rgba(255,255,255,0.4)] text-[14px] font-[500] mt-2">
            Narrow your search further by adding some filters.
          </p>
          <div
            onClick={() => setPopup(true)}
            className="bg-[#5C61F0] text-[white] text-[16px] rounded-[4px] px-8 py-2 mt-4 inline-block cursor-pointer transition duration-300 hover:scale-105"
          >
            Build Query
          </div>
        </div>
        <div className="pl-10">
          <div className="flex">
            <div className="relative">
              <img
                src={search}
                className="absolute left-[10px] top-[10px]"
                alt=""
              />
              <input
                type="text"
                placeholder="Search for product feedback"
                className="bg-[rgba(255,255,255,0.05)] text-[14px] rounded-[4px] border-[1px] border-[#404348] outline-none text-white py-[10px] pl-11 pr-6 w-[300px]"
              />
            </div>
            <div
              className="ml-5 bg-[rgba(255,255,255,0.05)] rounded-[4px] border-[1px] border-[#404348] py-[10px] px-6 cursor-pointer flex
                        transition duration-300 hover:scale-105"
            >
              <img src={download} className="mr-2" alt="" />
              <div className="text-[14px] text-[white]">Export Feedback</div>
            </div>
            <div className="ml-5 bg-[#5C61F0] rounded-[4px] py-[10px] px-8 cursor-pointer transition duration-300 hover:scale-105">
              <div className="text-[14px] text-[white]">View Feedback</div>
            </div>
            <div
              className="ml-5 bg-[rgba(255,255,255,0.05)] rounded-[4px] border-[1px] border-[#404348] py-[10px] px-6 cursor-pointer flex
                        transition duration-300 hover:scale-105"
            >
              <img src={calendar} className="mr-2" alt="" />
              <div className="text-[14px] text-[white]">
                Jan 01, 2023 - Mar 01, 2023
              </div>
            </div>
          </div>
          {queries.length > 0 ? (
            queries.map((q, i) => (
              <div
                key={i}
                className="bg-[#282B30] rounded-[4px] border-[1px] border-[#404348] p-4 mt-4 mb-6 flex justify-between items-center"
              >
                <div className="text-[white] font-[500] text-[16px] flex items-center justify-start">
                  <div className="border-[1px] border-[#404348] bg-[rgba(255,255,255,0.1)] rounded-[4px] flex items-center justify-center h-[35px] w-[35px]">
                    {i + 1}
                  </div>
                  <span className="ml-5">{q}</span>
                </div>
                <div
                  onClick={() => removeQuery(q, i)}
                  className="border-[1px] border-[#404348] bg-[rgba(255,255,255,0.1)] rounded-[4px] flex items-center justify-center cursor-pointer h-[35px] w-[35px]"
                >
                  <img src={deleteIcon} alt="" className="h-[24px] w-[24px]" />
                </div>
              </div>
            ))
          ) : (
            <></>
          )}
        </div>
      </div>
      {popup ? <QueryPopup setPopup={setPopup} /> : <></>}
    </div>
  );
}

export default App;
