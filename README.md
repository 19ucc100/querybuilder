# QueryBuilder

- Hi Nishant here 👋 Welcome to Query Builder !
- Its a web application which can build dynamic and complex queries simply.

## 🚀 Features

- Home page listing all queries generated till now
- Preview of the query being generated at the time of generating it
- Complex queries with group filters

## 🧰 Technologies Used

- ReactJS
- Typescript
- TailwindCSS

## 👀 Preview

* Link Link : https://buildmyquery.netlify.app/

![image](https://user-images.githubusercontent.com/56475750/216363994-2eedad0b-55b1-499c-a9d5-52a24eac4a13.png)

![image](https://user-images.githubusercontent.com/56475750/216364388-a19c3e8e-1f03-48b6-bdb8-dfbdc9a1b188.png)

